import { MediumPublication } from './medium.classes.publication';
import * as plugins from './medium.plugins';

export interface IMediumAccountData {
  id: string;
  username: string;
  url: string;
  imageUrl: string;
}

/**
 *
 */
export class MediumAccount implements IMediumAccountData {
  // STATIC

  // INSTANCE
  private accessToken: string;
  public readyDeferred = plugins.smartpromise.defer();
  public baseApiDomain = 'https://api.medium.com/v1';

  id: string;
  username: string;
  url: string;
  imageUrl: string;

  constructor(accessTokenArg: string) {
    this.accessToken = accessTokenArg;
    this.getAccountInfo().then((dataArg) => {
      Object.assign(this, dataArg);
      this.readyDeferred.resolve();
    });
  }

  public async getAccountInfo(): Promise<IMediumAccountData> {
    const result = await this.request('/me', 'GET');
    const accountData = result.body.data;
    return accountData;
  }

  public async getPublications(): Promise<MediumPublication[]> {
    return MediumPublication.getAllPublications(this);
  }

  public async getOwnPublications(): Promise<MediumPublication[]> {
    return MediumPublication.getOwnPublications(this);
  }

  public async request(routeArg: string, methodArg: 'POST' | 'GET', payloadArg?: any) {
    const response = await plugins.smartrequest.request(`${this.baseApiDomain}${routeArg}`, {
      headers: {
        Authorization: `Bearer ${this.accessToken}`,
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'Accept-Charset': 'utf-8',
      },
      method: methodArg,
      requestBody: payloadArg ? JSON.stringify(payloadArg) : null
    });
    return response;
  }
}
