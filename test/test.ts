import { expect, tap } from '@pushrocks/tapbundle';
import * as medium from '../ts/index';

import {Qenv} from '@pushrocks/qenv';

const testQenv = new Qenv('./', './.nogit/');
let testMediumAccount: medium.MediumAccount;

tap.test('first test', async () => {
  testMediumAccount = new medium.MediumAccount(testQenv.getEnvVarOnDemand('MEDIUM_API_TOKEN'));
  expect(testMediumAccount).to.be.instanceOf(medium.MediumAccount);
});

tap.test('should get me info', async () => {
  const result = await testMediumAccount.getAccountInfo();
  // console.log(result);
});

tap.test('should get publications', async () => {
  const result = await testMediumAccount.getPublications();
  // console.log(result);
});

tap.test('should get own publications', async () => {
  const result = await testMediumAccount.getOwnPublications();
  // console.log(result);
});

tap.start();
